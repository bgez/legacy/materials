from ... import ShaderMaterial
from ... import ReadFile

import bge

class Skybox(ShaderMaterial):
    '''The original work for this material comes from Martinsh:
    > http://devlog-martinsh.blogspot.ca/'''

    FragmentShader = ReadFile('./sky.fs', __file__)
    VertexShader = ReadFile('./sky.vs', __file__)

    def init(self):
        self.turbidity = 2.0
        self.reileigh = 2.5

    def pre_draw(self, shader):
        shader.setAttrib(bge.logic.SHD_TANGENT)
        shader.setUniformDef('ModelMatrix', bge.logic.MODELMATRIX)
        shader.setUniformDef('cameraPos', bge.logic.CAM_POS)
        shader.setUniform1f('turbidity', self.turbidity)
        shader.setUniform1f('reileigh', self.reileigh)
        #shader.setUniform1f('luminance', bge.logic.luminance)
        #shader.setUniform1f('bias', bge.logic.bias)
        #shader.setUniform1f('contrast', bge.logic.contrast)
        #shader.setUniform1f('lumamount', bge.logic.lumamount)
