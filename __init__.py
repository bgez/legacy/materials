from .objects.MaterialManager import *
from .objects.ShaderMaterial import *

# Utility function
def ReadFile(path, relative=None, m='r'):
    import os.path
    if relative:
        path = os.path.join(os.path.dirname(relative), path)
    with open(path, m) as file:
        return file.read()

# Run the manager
MaterialManager.RegisterEvents()
